import numpy as np

def get_reverse_merge_order(tree):
    """
    tree: binary tree of embedded lists

    notably trees do not have heads in this formalism. if needed, heads should be supplied to nodes in a specifier position:
    e.g. [<NP> [[<DET> the] [<N> dog]]

    TODO:
    * checking binary branching
    * checking types
    """

    tree_flatten = tree.flatten()

    merge_list=[None]*(len(tree_flatten)*2-1) # create list of possible merge "slots" of length max_height*2-1

    next_merge = 1 # next available slot with min index, skipping 0 for hardcoded root
    node_stack = [[0,tree]] # list of unresolved nodes, preloaded with root at index 0
    while node_stack:
        merge_index,node = node_stack.pop(-1) # take unresolved node from work stack
        merge_item = [] # prepare item to be added to merge_list

        assert len(node) == 2, f"Node {node} should be binary, is {len(node)}-ary."

        for child in node:
            if type(child) == str: # TODO change type here for idk maybe not int?
                merge_item.append(child) # if leaf, add leaf to merge_item
            else:
                # add placeholder to merge_item
                merge_item.append(next_merge)
                # add index of placeholder and node to node_stack
                node_stack.append([next_merge,child])
                next_merge+=1 #incr. min slot

        # add merge_item to merge_list
        merge_list[merge_index] = merge_item

    # invert order and remove trailing Nones
    merge_order = [x for x in merge_list if x]
    #print(merge_order)
    return merge_order

if __name__ == "__main__":

    # Unit tests: get_reverse_merge_order()
    test_tree = np.array([["a","b"],[[["c","d"],"e"], "f"]], dtype=object)
    reverse_merge_order = get_reverse_merge_order(test_tree)
    assert reverse_merge_order == [[1,2],['a','b'],[3,'f'],[4,'e'],['c','d']]

    test_tree = np.array(["a","b"], dtype=object)
    reverse_merge_order = get_reverse_merge_order(test_tree)
    assert reverse_merge_order == [["a","b"]]