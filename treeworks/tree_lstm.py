#!/home/zimmermann/anaconda3/envs/treeworks/bin/env python
# -*- coding: utf-8 -*-

import os
import math

import deprecation

import spacy

import torch
from torch import optim, nn, utils, Tensor
#from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
import pytorch_lightning as pl

from nltk.tree import tree, chomsky_normal_form as cnf


class BinaryTreeLSTM(nn.Module):
    """ Tree-LSTM that processes trees in Chomsky-Normal-Form (CNF). At each node the embeddings of sisters are predicted, final layer can be used for downstream tasks and grounded training.

    """
    def __init__(self, input_dim:int, hidden_dim:int, restricted=False, filters=[], device='cpu'):
        super(BinaryTreeLSTM, self).__init__()
        
        self.embedding = nn.Embedding(10,500)

        self.loss = nn.MSELoss()

        self.predictor_0 = nn.Sequential(nn.Linear(input_dim, hidden_dim), nn.ReLU(), nn.Linear(hidden_dim, input_dim))
        self.predictor_1 = nn.Sequential(nn.Linear(input_dim, hidden_dim), nn.ReLU(), nn.Linear(hidden_dim, input_dim))

        self.restricted = restricted
        self.filters = filters
        self.filter_dim = len(filters)
        self.filter_selector = nn.Sequential(nn.Linear(input_dim,hidden_dim), nn.ReLU(), nn.Linear(hidden_dim, self.filter_dim))

        self.idim = input_dim
        self.hdim = hidden_dim
        
        self.final_layer = nn.Linear(self.hdim, 1) # map from hidden layer to output
        self.zero_x = torch.zeros(1, self.hdim, requires_grad=False)
              
        #i_t
        self.U_i = nn.Parameter(Tensor(input_dim, hidden_dim))
        self.V_i0 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.V_i1 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.b_i = nn.Parameter(Tensor(hidden_dim))
        
        #f_t
        self.U_f = nn.Parameter(Tensor(input_dim, hidden_dim))
        self.V_f00 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.V_f01 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.V_f10 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.V_f11 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.b_f = nn.Parameter(Tensor(hidden_dim))

        #c_t
        self.U_c = nn.Parameter(Tensor(input_dim, hidden_dim))
        self.V_c0 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.V_c1 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.b_c = nn.Parameter(Tensor(hidden_dim))
        
        #o_t
        self.U_o = nn.Parameter(Tensor(input_dim, hidden_dim))
        self.V_o0 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.V_o1 = nn.Parameter(Tensor(hidden_dim, hidden_dim))
        self.b_o = nn.Parameter(Tensor(hidden_dim))
        
        self.init_weights()

    def init_weights(self):
        stdv = 1.0 / math.sqrt(self.hdim)
        for weight in self.parameters():
            weight.data.uniform_(-stdv, stdv)
    
    def forward(self, sent, action_sequence, mapping, init_states=None):
        
        """
        assumes x.shape represents (batch_size, sequence_size, input_size)
        """
        
        stack = []
        loss = []

        n = len(action_sequence)

        for x in sent:

            x = torch.tensor([mapping[x]], dtype=torch.long)

            x = self.embedding(x)
            #x = x.view(1, x.shape[0])
            cx = x
            hx = torch.tanh(cx)
            stack.append((self.zero_x, (hx, cx)))

        for action in action_sequence:
            if type(action)==int:
                x, (hx, cx) = stack[action]
                stack[action] = (self.zero_x, (hx, cx))

            if type(action)==tuple:
                x_t0, (h_t0, c_t0) = stack[action[0]]
                x_t1, (h_t1, c_t1) = stack[action[1]]
                
                x_t = self.zero_x

                h_t1_hat = self.predictor_0(h_t0)
                h_t0_hat = self.predictor_1(h_t1)

                loss.append(self.loss(h_t0_hat,h_t0))
                loss.append(self.loss(h_t1_hat,h_t1))


                    
                i_t = torch.sigmoid(x_t @ self.U_i + h_t0 @ self.V_i0 + h_t1 @ self.V_i1 + self.b_i)
                f_t0 = torch.sigmoid(x_t @ self.U_f + h_t0 @ self.V_f00 + h_t1 @ self.V_f01 + self.b_f)
                f_t1 = torch.sigmoid(x_t @ self.U_f + h_t0 @ self.V_f10 + h_t1 @ self.V_f11 + self.b_f)
                g_t = torch.tanh(x_t @ self.U_c + h_t0 @ self.V_c0 + h_t1 @ self.V_c1 + self.b_c)
                o_t = torch.sigmoid(x_t @ self.U_o + h_t0 @ self.V_o0 + h_t1 @ self.V_o1 + self.b_o)
                c_t = i_t * g_t + (f_t0 * c_t0) + (f_t1 * c_t1)   #f_t * c_t + i_t * g_t
                h_t = o_t * torch.tanh(c_t)

                if self.restricted: 
                    h_t = self.filters[torch.argmax(self.filter_selector(h_t))](h_t0, h_t1)

                stack[action[0]] = (self.zero_x, (h_t, c_t))
                del stack[action[1]]

        assert len(stack)==1
        x, (h_t0, c_t0) = stack[0]
        score = self.final_layer(hx)

        return score, loss





@deprecation.deprecated(details="This is copy-pasted code from GitHub, use the custom TreeLSTM instead.")
class ChildSumTreeLSTM(torch.nn.Module):
    """PyTorch Child-Sum Tree LSTM model

    See Tai et al. 2015 https://arxiv.org/abs/1503.00075 for model description.
    PyTorch TreeLSTM model that implements efficient batching.
    """

    def __init__(self, in_features, out_features):
        '''TreeLSTM class initializer

        Takes in int sizes of in_features and out_features and sets up model Linear network layers.
        '''
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features

        # bias terms are only on the W layers for efficiency
        self.W_iou = torch.nn.Linear(self.in_features, 3 * self.out_features)
        self.U_iou = torch.nn.Linear(self.out_features, 3 * self.out_features, bias=False)

        # f terms are maintained seperate from the iou terms because they involve sums over child nodes
        # while the iou terms do not
        self.W_f = torch.nn.Linear(self.in_features, self.out_features)
        self.U_f = torch.nn.Linear(self.out_features, self.out_features, bias=False)

    def forward(self, features, node_order, adjacency_list, edge_order):
        '''Run TreeLSTM model on a tree data structure with node features

        Takes Tensors encoding node features, a tree node adjacency_list, and the order in which 
        the tree processing should proceed in node_order and edge_order.
        '''

        # Total number of nodes in every tree in the batch
        batch_size = node_order.shape[0]

        # Retrive device the model is currently loaded on to generate h, c, and h_sum result buffers
        device = next(self.parameters()).device

        # h and c states for every node in the batch
        h = torch.zeros(batch_size, self.out_features, device=device)
        c = torch.zeros(batch_size, self.out_features, device=device)

        # populate the h and c states respecting computation order
        for n in range(node_order.max() + 1):
            self._run_lstm(n, h, c, features, node_order, adjacency_list, edge_order)

        return h, c

    def _run_lstm(self, iteration, h, c, features, node_order, adjacency_list, edge_order):
        '''Helper function to evaluate all tree nodes currently able to be evaluated.
        '''
        # N is the number of nodes in the tree
        # n is the number of nodes to be evaluated on in the current iteration
        # E is the number of edges in the tree
        # e is the number of edges to be evaluated on in the current iteration
        # F is the number of features in each node
        # M is the number of hidden neurons in the network

        # node_order is a tensor of size N x 1
        # edge_order is a tensor of size E x 1
        # features is a tensor of size N x F
        # adjacency_list is a tensor of size E x 2

        # node_mask is a tensor of size N x 1
        node_mask = node_order == iteration
        # edge_mask is a tensor of size E x 1
        edge_mask = edge_order == iteration

        # x is a tensor of size n x F
        x = features[node_mask, :]

        # At iteration 0 none of the nodes should have children
        # Otherwise, select the child nodes needed for current iteration
        # and sum over their hidden states
        if iteration == 0:
            iou = self.W_iou(x)
        else:
            # adjacency_list is a tensor of size e x 2
            adjacency_list = adjacency_list[edge_mask, :]

            # parent_indexes and child_indexes are tensors of size e x 1
            # parent_indexes and child_indexes contain the integer indexes needed to index into
            # the feature and hidden state arrays to retrieve the data for those parent/child nodes.
            parent_indexes = adjacency_list[:, 0]
            child_indexes = adjacency_list[:, 1]

            # child_h and child_c are tensors of size e x 1
            child_h = h[child_indexes, :]
            child_c = c[child_indexes, :]

            # Add child hidden states to parent offset locations
            _, child_counts = torch.unique_consecutive(parent_indexes, return_counts=True)
            child_counts = tuple(child_counts)

            parent_children = torch.split(child_h, child_counts)
            parent_list = [item.sum(0) for item in parent_children]

            h_sum = torch.stack(parent_list)
            iou = self.W_iou(x) + self.U_iou(h_sum)

        # i, o and u are tensors of size n x M
        i, o, u = torch.split(iou, iou.size(1) // 3, dim=1)
        i = torch.sigmoid(i)
        o = torch.sigmoid(o)
        u = torch.tanh(u)

        # At iteration 0 none of the nodes should have children
        # Otherwise, calculate the forget states for each parent node and child node
        # and sum over the child memory cell states
        if iteration == 0:
            c[node_mask, :] = i * u
        else:
            # f is a tensor of size e x M
            f = self.W_f(features[parent_indexes, :]) + self.U_f(child_h)
            f = torch.sigmoid(f)

            # fc is a tensor of size e x M
            fc = f * child_c

            # Add the calculated f values to the parent's memory cell state
            parent_children = torch.split(fc, child_counts)
            parent_list = [item.sum(0) for item in parent_children]

            c_sum = torch.stack(parent_list)
            c[node_mask, :] = i * u + c_sum

        h[node_mask, :] = o * torch.tanh(c[node_mask])